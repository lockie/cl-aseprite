(pushnew :docs *features*)
(ql:quickload '(:staple :staple-markdown
                :cl-aseprite :parachute))
(staple:generate :cl-aseprite
                 :if-exists :supersede
                 :output-directory #P"public/"
                 :images '(#P"docs/logo.svg"))
