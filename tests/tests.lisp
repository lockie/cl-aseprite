(in-package #:cl-aseprite/tests)


(defun test-file (name)
  (merge-pathnames (format nil "tests/files/~a.aseprite" name)
                   (asdf:component-pathname (asdf:find-system :cl-aseprite))))

(define-test load-from-pathname
  :parent cl-aseprite
  (true (cl-aseprite::sprite-p (load-sprite (test-file "empty")))))

(define-test load-from-string
  :parent cl-aseprite
  (true (cl-aseprite::sprite-p (load-sprite (namestring (test-file "empty"))))))

(define-test load-from-stream
  :parent cl-aseprite
  (with-open-file (stream (test-file "empty")
                          :element-type '(unsigned-byte 8))
    (true (cl-aseprite::sprite-p (load-sprite stream)))))

(define-test no-load-from-non-file
  :parent cl-aseprite
  (fail (load-sprite 42)))

(define-test stream-not-closed-in-load
  :parent cl-aseprite
  (with-open-file (stream (test-file "empty")
                          :element-type '(unsigned-byte 8))
    (load-sprite stream)
    (is > 0 (file-position stream))))

(define-test wrong-magic
  :parent cl-aseprite
  (fail (load-sprite (test-file "wrong-magic"))))

(define-test wrong-frame-magic
  :parent cl-aseprite
  (fail (load-sprite (test-file "wrong-frame-magic"))))

(define-test sprite-metadata
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "empty"))))
    (is =       32    (sprite-width             sprite))
    (is =       32    (sprite-height            sprite))
    (is =       32    (sprite-color-depth       sprite))
    (is eq      :srgb (sprite-color-profile     sprite))
    (is eq      t     (sprite-has-opacity-p     sprite))
    (is =       0     (sprite-transparent-index sprite))
    (is =       1     (sprite-colors            sprite))
    (is =       1     (sprite-pixel-ratio       sprite))
    (is string= ""    (sprite-user-data         sprite))
    (is =       1     (sprite-frames            sprite))
    (is eq      nil   (sprite-tags              sprite))))

(define-test grayscale
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "grayscale"))))
    (is = 16 (sprite-color-depth sprite))))

(define-test indexed
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "indexed"))))
    (is = 8 (sprite-color-depth sprite))
    (is = 1 (sprite-transparent-index sprite))))

(define-test color-profile
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "color-profile"))))
    (is eq nil (sprite-color-profile sprite))))

(define-test wide
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "wide"))))
    (is = 2 (sprite-pixel-ratio sprite))))

(define-test high
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "high"))))
    (is = 1/2 (sprite-pixel-ratio sprite))))

(define-test sprite-userdata
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "sprite-userdata"))))
    (is string= "userdata" (sprite-user-data sprite))))

(define-test palette
  :parent cl-aseprite
  (let ((sprite (load-sprite (test-file "palette"))))
    (is equalp #(#xff000000 #xffffffff 0) (sprite-palette sprite))))

(define-test layers
  :parent cl-aseprite
  (let* ((sprite (load-sprite (test-file "layer")))
         (layers (sprite-layers sprite))
         (layer1 (getf layers :layer1))
         (layer2 (getf layers :layer2))
         (group1 (getf layers :group1))
         (reference1 (getf layers :referencelayer1)))
    (is eq      nil        (layer-visiblep      layer1))
    (is eq      nil        (layer-backgroundp   layer1))
    (is eq      t          (layer-continuousp   layer1))
    (is eq      nil        (layer-referencep    layer1))
    (is eq      :image     (layer-type          layer1))
    (is =       0          (layer-child-level   layer1))
    (is eq      :overlay   (layer-blend-mode    layer1))
    (is =       107        (layer-opacity       layer1))
    (is eq      nil        (layer-tileset-index layer1))
    (is string= "userdata" (layer-user-data     layer1))
    (is equalp  #(nil)     (layer-cels          layer1))

    (is eq      t          (layer-visiblep      layer2))
    (is eq      nil        (layer-backgroundp   layer2))
    (is eq      nil        (layer-continuousp   layer2))
    (is eq      nil        (layer-referencep    layer2))
    (is eq      :image     (layer-type          layer2))
    (is =       1          (layer-child-level   layer2))
    (is eq      :normal    (layer-blend-mode    layer2))
    (is =       255        (layer-opacity       layer2))
    (is eq      nil        (layer-tileset-index layer2))
    (is string= ""         (layer-user-data     layer2))
    (is equalp  #(nil)     (layer-cels          layer2))

    (is eq      t          (layer-visiblep      group1))
    (is eq      nil        (layer-backgroundp   group1))
    (is eq      nil        (layer-continuousp   group1))
    (is eq      nil        (layer-referencep    group1))
    (is eq      :group     (layer-type          group1))
    (is =       0          (layer-child-level   group1))
    (is eq      :normal    (layer-blend-mode    group1))
    (is =       0          (layer-opacity       group1))
    (is eq      nil        (layer-tileset-index group1))
    (is string= ""         (layer-user-data     group1))
    (is equalp  #(nil)     (layer-cels          group1))

    (is eq      t          (layer-visiblep      reference1))
    (is eq      nil        (layer-backgroundp   reference1))
    (is eq      nil        (layer-continuousp   reference1))
    (is eq      t          (layer-referencep    reference1))
    (is eq      :image     (layer-type          reference1))
    (is =       0          (layer-child-level   reference1))
    (is eq      :normal    (layer-blend-mode    reference1))
    (is =       255        (layer-opacity       reference1))
    (is eq      nil        (layer-tileset-index reference1))
    (is string= ""         (layer-user-data     reference1))
    (is equalp  #(nil)     (layer-cels          reference1))))

(define-test cel
  :parent cl-aseprite
  (let* ((sprite (load-sprite (test-file "cel")))
         (layer (getf (sprite-layers sprite) :layer1))
         (cel (aref (layer-cels layer) 0)))
    (is =       150        (cel-duration              cel))
    (is =       0          (cel-x                     cel))
    (is =       0          (cel-y                     cel))
    (is =       128        (cel-opacity               cel))
    (is =       1          (cel-z-index               cel))
    (is eq      :image     (cel-type                  cel))
    (is string= "userdata" (cel-user-data             cel))
    (is =       2          (cel-width                 cel))
    (is =       2          (cel-height                cel))
    (is equalp
        #(0 0 0 255 0 0 0 255 255 255 255 255 0 0 0 255)
        (cel-data cel))
    (is =       0          (cel-frame-position        cel))
    (is =       0          (cel-id-bitmask            cel))
    (is =       0          (cel-x-flip-bitmask        cel))
    (is =       0          (cel-y-flip-bitmask        cel))
    (is =       0          (cel-diagonal-flip-bitmask cel))))

(define-test linked-cels
  :parent cl-aseprite
  (let* ((sprite (load-sprite (test-file "linked-cels")))
         (layer (getf (sprite-layers sprite) :layer1))
         (cel (aref (layer-cels layer) 2)))
    (is eq :linked (cel-type           cel))
    (is eq nil     (cel-data           cel))
    (is =  1       (cel-frame-position cel))))

(define-test tags
  :parent cl-aseprite
  (let* ((sprite (load-sprite (test-file "tags")))
         (tags (sprite-tags sprite))
         (tag1 (getf tags :tag1))
         (tag2 (getf tags :tag2))
         (tag3 (getf tags :tag3))
         (tag4 (getf tags :tag4)))
    (is =       0                  (tag-first-frame tag1))
    (is =       1                  (tag-last-frame  tag1))
    (is eq      :forward           (tag-direction   tag1))
    (is =       0                  (tag-repeat      tag1))
    (is string= "userdata1"        (tag-user-data   tag1))

    (is =       1                  (tag-first-frame tag2))
    (is =       2                  (tag-last-frame  tag2))
    (is eq      :reverse           (tag-direction   tag2))
    (is =       12                 (tag-repeat      tag2))
    (is string= ""                 (tag-user-data   tag2))

    (is =       3                  (tag-first-frame tag3))
    (is =       4                  (tag-last-frame  tag3))
    (is eq      :ping-pong         (tag-direction   tag3))
    (is =       1                  (tag-repeat      tag3))
    (is string= "userdata3"        (tag-user-data   tag3))

    (is =       0                  (tag-first-frame tag4))
    (is =       4                  (tag-last-frame  tag4))
    (is eq      :ping-pong-reverse (tag-direction   tag4))
    (is =       0                  (tag-repeat      tag4))
    (is string= ""                 (tag-user-data   tag4))))
