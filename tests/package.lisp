(in-package #:cl-user)


(defpackage cl-aseprite/tests
  (:use #:cl #:cl-aseprite #:parachute)
  (:export #:run))
