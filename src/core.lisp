(in-package #:cl-aseprite.core)


(declaim (type fixnum *chunk-size* *color-depth*))
(defvar *chunk-size*)
(defvar *color-depth*)

(defbinary fixed-point
    ()
    (numerator   0 :type (unsigned-byte 16))
    (denominator 0 :type (unsigned-byte 16)))

(defbinary point
    ()
    (x 0 :type (signed-byte 32))
    (y 0 :type (signed-byte 32)))

(defbinary size
    ()
    (width  0 :type (signed-byte 32))
    (height 0 :type (signed-byte 32)))

(defbinary rect
    ()
    (origin nil :type point)
    (size   nil :type size))

(define-enum chunk-type 2 ()
             (:old-palette   #x0004)
             (:layer         #x2004)
             (:cel           #x2005)
             (:color-profile #x2007)
             (:tags          #x2018)
             (:palette       #x2019)
             (:user-data     #x2020))

(defbinary old-palette-color
    ()
    (r 0 :type (unsigned-byte 8))
    (g 0 :type (unsigned-byte 8))
    (b 0 :type (unsigned-byte 8)))

(defbinary old-palette-packet
    ()
    (skip   0   :type (unsigned-byte 8))
    (size   0   :type (unsigned-byte 8))
    (colors nil :type (simple-array old-palette-color
                                    ((if (zerop size) 256 size)))))

(defbinary old-palette-chunk-data
    ()
    (size    0   :type (unsigned-byte 16))
    (packets nil :type (simple-array old-palette-packet (size))))

(define-enum layer-type 2 ()
             (:image   0)
             (:group   1)
             (:tilemap 2))

(define-enum layer-blend-mode 2 ()
             (:normal      0)
             (:multiply    1)
             (:screen      2)
             (:overlay     3)
             (:darken      4)
             (:lighten     5)
             (:color-dodge 6)
             (:color-burn  7)
             (:hard-light  8)
             (:soft-light  9)
             (:difference  10)
             (:exclusion   11)
             (:hue         12)
             (:saturation  13)
             (:color       14)
             (:luminosity  15)
             (:addition    16)
             (:subtract    17)
             (:divide      18))

(defbinary layer-chunk-data
    ()
    ((visible editable locked background continuous group-collapsed reference
              reserved*)
     (0 0 0 0 0 0 0 0) :type (bit-field :raw-type (unsigned-byte 16)
                                        :member-types ((unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 1)
                                                       (unsigned-byte 9))))
    (type           0   :type layer-type)
    (child-level    0   :type (unsigned-byte 16))
    (default-width  0   :type (unsigned-byte 16))
    (default-height 0   :type (unsigned-byte 16))
    (blend-mode     0   :type layer-blend-mode)
    (opacity        0   :type (unsigned-byte 8))
    (reserved       0   :type (unsigned-byte 24))
    (name           ""  :type (counted-string 2 :external-format :utf8))
    (tileset-index  nil :type (eval (case type
                                      (:tilemap '(unsigned-byte 32))
                                      (t        'null)))))

(define-enum cel-type 2 ()
             (:raw                0)
             (:linked             1)
             (:compressed-image   2)
             (:compressed-tilemap 3))

(defbinary raw-cel-data
    ()
    (width  0 :type (unsigned-byte 16))
    (height 0 :type (unsigned-byte 16))
    (data   nil :type (simple-array (unsigned-byte 8)
                                    ((* width height *color-depth*)))))

(defbinary linked-cel-data
    ()
    (position 0 :type (unsigned-byte 16)))

(defbinary compressed-image-cel-data
    ()
    (width  0   :type (unsigned-byte 16))
    (height 0   :type (unsigned-byte 16))
    (data   nil :type custom
                :reader
                (lambda (stream)
                  (let* ((input-size (- *chunk-size* 20))
                         (output-size (* width height *color-depth*))
                         (data (decompress nil 'chipz:zlib
                                           (read-bytes input-size stream)
                                           :buffer-size output-size)))
                    (values data input-size)))))

(defbinary compressed-tilemap-cel-data
    ()
    (width                 0   :type (unsigned-byte 16))
    (height                0   :type (unsigned-byte 16))
    (bits-per-tile         0   :type (unsigned-byte 16))
    (id-bitmask            0   :type (unsigned-byte 32))
    (x-flip-bitmask        0   :type (unsigned-byte 32))
    (y-flip-bitmask        0   :type (unsigned-byte 32))
    (diagonal-flip-bitmask 0   :type (unsigned-byte 32))
    (reserved              nil :type (simple-array (unsigned-byte 8) (10)))
    (data                  nil :type custom
                               :reader
                               (lambda (stream)
                                 (let* ((input-size (- *chunk-size* 48))
                                        (output-size
                                          (* width height
                                             (floor bits-per-tile 8)))
                                        (data (decompress
                                               nil 'chipz:zlib
                                               (read-bytes input-size stream)
                                               :buffer-size output-size)))
                                   (values data input-size)))))

(defbinary cel-chunk-data
    ()
    (layer-index 0   :type (unsigned-byte 16))
    (x-position  0   :type (signed-byte 16))
    (y-position  0   :type (signed-byte 16))
    (opacity     0   :type (unsigned-byte 8))
    (type        nil :type cel-type)
    (z-index     0   :type (signed-byte 16))
    (reserved    nil :type (simple-array (unsigned-byte 8) (5)))
    (data        nil :type
                 (eval
                  (case type
                    (:raw                'raw-cel-data)
                    (:linked             'linked-cel-data)
                    (:compressed-image   'compressed-image-cel-data)
                    (:compressed-tilemap 'compressed-tilemap-cel-data)))))

(define-enum color-profile-type 2 ()
             (:no-color-profile 0)
             (:srgb             1)
             (:icc              2))

(defbinary icc-data
    ()
    (size 0   :type (unsigned-byte 32))
    (data nil :type (simple-array (unsigned-byte 8) (size))))

(defbinary color-profile-chunk-data
    ()
    (type     nil :type color-profile-type)
    (flags    0   :type (unsigned-byte 16))
    (gamma    nil :type fixed-point)
    (reserved nil :type (simple-array (unsigned-byte 8) (8)))
    (icc-data nil :type (eval (case type
                                (icc 'icc-data)
                                (t   'null)))))

(define-enum animation-direction 1 ()
             (:forward           0)
             (:reverse           1)
             (:ping-pong         2)
             (:ping-pong-reverse 3))

(defbinary tag
    ()
    (first-frame 0   :type (unsigned-byte 16))
    (last-frame  0   :type (unsigned-byte 16))
    (direction   nil :type animation-direction)
    (repeat      0   :type (unsigned-byte 16))
    (reserved    nil :type (simple-array (unsigned-byte 8) (6)))
    (r           0   :type (unsigned-byte 8))
    (g           0   :type (unsigned-byte 8))
    (b           0   :type (unsigned-byte 8))
    (a           0   :type (unsigned-byte 8))
    (name        ""  :type (counted-string 2 :external-format :utf8)))

(defbinary tags-chunk-data
    ()
    (size     0   :type (unsigned-byte 16))
    (reserved nil :type (simple-array (unsigned-byte 8) (8)))
    (tags     nil :type (simple-array tag (size))))

(defbinary palette-entry
    ()
    (flags 0  :type (unsigned-byte 16))
    (r     0  :type (unsigned-byte 8))
    (g     0  :type (unsigned-byte 8))
    (b     0  :type (unsigned-byte 8))
    (a     0  :type (unsigned-byte 8))
    (name  "" :type (eval
                     (case (logand flags 1)
                       (1 '(counted-string 2 :external-format :utf8))
                       (0 '(counted-string 0))))))

(defbinary palette-chunk-data
    ()
    (size        0   :type (unsigned-byte 32))
    (first-index 0   :type (unsigned-byte 32))
    (last-index  0   :type (unsigned-byte 32))
    (reserved    nil :type (simple-array (unsigned-byte 8) (8)))
    (entries     nil :type (simple-array palette-entry
                                         ((- last-index first-index -1)))))

(defbinary text-user-data
    ()
    (text "" :type (counted-string 2 :external-format :utf8)))

(defbinary color-user-data
    ()
    (r 0 :type (unsigned-byte 8))
    (g 0 :type (unsigned-byte 8))
    (b 0 :type (unsigned-byte 8))
    (a 0 :type (unsigned-byte 8)))

(defbinary value
    ()
    (type 0   :type (unsigned-byte 16))
    (data nil :type (eval (case type
                            (#x0001 '(unsigned-byte 8))
                            (#x0002 '(signed-byte 8))
                            (#x0003 '(unsigned-byte 8))
                            (#x0004 '(signed-byte 16))
                            (#x0005 '(unsigned-byte 16))
                            (#x0006 '(signed-byte 32))
                            (#x0007 '(unsigned-byte 32))
                            (#x0008 '(signed-byte 64))
                            (#x0009 '(unsigned-byte 64))
                            (#x000A 'fixed-point)
                            (#x000B 'single-float)
                            (#x000C 'double-float)
                            (#x000D '(counted-string 2 :external-format :utf8))
                            (#x000E 'point)
                            (#x000F 'size)
                            (#x0010 'rect)
                            (#x0011 'vector*)
                            (#x0012 'properties-map*)
                            (#x0013 '(simple-array (unsigned-byte 8) (16)))))))

(defbinary vector*
    ()
    (size 0   :type (unsigned-byte 32))
    (type 0   :type (unsigned-byte 16))
    (data nil :type (eval (case type
                            (#x0000 '(simple-array value (size)))
                            (#x0001 '(simple-array (unsigned-byte 8) (size)))
                            (#x0002 '(simple-array (signed-byte 8) (size)))
                            (#x0003 '(simple-array (unsigned-byte 8) (size)))
                            (#x0004 '(simple-array (signed-byte 16) (size)))
                            (#x0005 '(simple-array (unsigned-byte 16) (size)))
                            (#x0006 '(simple-array (signed-byte 32) (size)))
                            (#x0007 '(simple-array (unsigned-byte 32) (size)))
                            (#x0008 '(simple-array (signed-byte 64) (size)))
                            (#x0009 '(simple-array (unsigned-byte 64) (size)))
                            (#x000A '(simple-array fixed-point (size)))
                            (#x000B '(simple-array single-float (size)))
                            (#x000C '(simple-array double-float (size)))
                            (#x000D '(simple-array (counted-string 2
                                                    :external-format :utf8)
                                      (size)))
                            (#x000E '(simple-array point (size)))
                            (#x000F '(simple-array size (size)))
                            (#x0010 '(simple-array rect (size)))
                            (#x0011 '(simple-array vector* (size)))
                            (#x0012 '(simple-array properties-map* (size)))))))

(defbinary property
    ()
    (name  ""  :type (counted-string 2 :external-format :utf8))
    (value nil :type value))

(defbinary properties-map*
    ()
    (size       0   :type (unsigned-byte 32))
    (properties nil :type (simple-array property (size))))

(defbinary properties-map
    ()
    (key        0   :type (unsigned-byte 32))
    (size       0   :type (unsigned-byte 32))
    (properties nil :type (simple-array property (size))))

(defbinary properties-user-data
    ()
    (size            0   :type (unsigned-byte 32))
    (maps-size       0   :type (unsigned-byte 32))
    (properties-maps nil :type (simple-array properties-map (maps-size))))

(defbinary user-data-chunk-data
    ()
    ((has-text has-color has-properties reserved) (0 0 0 0)
     :type (bit-field :raw-type (unsigned-byte 32)
                      :member-types ((unsigned-byte 1)
                                     (unsigned-byte 1)
                                     (unsigned-byte 1)
                                     (unsigned-byte 29))))
    (text       nil :type (eval (case has-text
                                  (1 'text-user-data)
                                  (0 'null))))
    (color      nil :type (eval (case has-color
                                  (1 'color-user-data)
                                  (0 'null))))
    (properties nil :type (eval (case has-properties
                                  (1 'properties-user-data)
                                  (0 'null)))))

(defbinary chunk
    ()
    (size 0   :type (unsigned-byte 32)
              :reader
              (lambda (stream)
                (let ((size (read-integer 4 stream)))
                (setf *chunk-size* (- size 6))
                  (values size 4))))
    (type nil :type chunk-type)
    (data nil :type (eval
                     (case type
                       (:old-palette   'old-palette-chunk-data)
                       (:layer         'layer-chunk-data)
                       (:cel           'cel-chunk-data)
                       (:color-profile 'color-profile-chunk-data)
                       (:tags          'tags-chunk-data)
                       (:palette       'palette-chunk-data)
                       (:user-data     'user-data-chunk-data)))))

(defbinary frame
    ()
    (bytes      0   :type (unsigned-byte 32))
    (magic      0   :type (magic :actual-type (unsigned-byte 16) :value #xF1FA))
    (old-chunks 0   :type (unsigned-byte 16))
    (duration   0   :type (unsigned-byte 16))
    (reserved   0   :type (unsigned-byte 16))
    (new-chunks 0   :type (unsigned-byte 32))
    (chunks     nil :type (simple-array chunk ((max old-chunks new-chunks)))))

(defbinary file
    ()
    (file-size         0   :type (unsigned-byte 32))
    (magic             0   :type (magic :actual-type (unsigned-byte 16)
                                        :value #xA5E0))
    (frames-size       0   :type (unsigned-byte 16))
    (width             0   :type (unsigned-byte 16))
    (height            0   :type (unsigned-byte 16))
    (color-depth       0   :type (unsigned-byte 16)
                           :reader
                           (lambda (stream)
                             (let ((color-depth (read-integer 2 stream)))
                               (setf *color-depth* (floor color-depth 8))
                               (values color-depth 2))))
    (flags             0   :type (unsigned-byte 32))
    (speed             0   :type (unsigned-byte 16))
    (reserved-1        0   :type (simple-array (unsigned-byte 8) (8)))
    (transparent-index 0   :type (unsigned-byte 8))
    (reserved-2        0   :type (simple-array (unsigned-byte 8) (3)))
    (colors            0   :type (unsigned-byte 16))
    (pixel-width       0   :type (unsigned-byte 8))
    (pixel-height      0   :type (unsigned-byte 8))
    (grid-x            0   :type (signed-byte 16))
    (grid-y            0   :type (signed-byte 16))
    (grid-width        0   :type (unsigned-byte 16))
    (grid-height       0   :type (unsigned-byte 16))
    (reserved-3        0   :type (simple-array (unsigned-byte 8) (84)))
    (frames            nil :type (simple-array frame (frames-size))))
