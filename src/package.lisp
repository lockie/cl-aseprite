(in-package #:cl-user)


(defpackage cl-aseprite.core
  (:use #:cl)
  (:import-from #:chipz #:decompress)
  (:import-from #:lisp-binary
                #:bit-field #:counted-string #:custom #:defbinary #:define-enum
                #:magic #:read-binary #:read-bytes #:read-counted-string
                #:read-integer))

(defpackage cl-aseprite
  (:documentation "**NOTE: this software is of alpha quality, and the API is
subject to change.**

`cl-aseprite` is a high-performance Common Lisp library for parsing
`.aseprite` files created with [Aseprite](https://aseprite.org) and
[LibreSprite](https://libresprite.github.io) programs into meaningful Lisp
structures.

Aseprite is a proprietary image editor designed for pixel art drawing and
animation. It was used in the development of several notable games such as
Celeste, Loop Hero, and many others (source:
[Wikipedia](https://en.wikipedia.org/wiki/Aseprite)).

LibreSprite is a free and open source fork of Aseprite created after its license changed from GPLv2 to proprietary in 2016.

Key part of the library is `LOAD-SPRITE` function, which returns `SPRITE`
structure, which in turn contains lists of `TAG` and `LAYER` structures with
latter having an array of `CEL` structures. See [Aseprite
docs](https://aseprite.org/docs) for details on those entities.

In most cases, `CEL`'s `DATA` field contains an array of 32-bit ABGR pixel
data, so to convert it to image just copy it using `CFFI` into proper
container, e.g. to the result of call to
[al_lock_bitmap](https://liballeg.org/a5docs/trunk/graphics.html#al_lock_bitmap)
when using [cl-liballegro](https://github.com/resttime/cl-liballegro).
")
  (:nicknames #:aseprite #:ase)
  (:use #:cl #:let-plus)
  (:import-from #:alexandria #:format-symbol #:if-let)
  (:import-from #:lisp-binary #:read-binary)
  (:import-from #:trivial-types #:property-list)
  (:local-nicknames (#:core #:cl-aseprite.core))
  (:export
   ;; tag
   #:tag
   #:tag-first-frame
   #:tag-last-frame
   #:tag-name
   #:tag-direction
   #:tag-repeat
   #:tag-user-data
   ;; cel
   #:cel
   #:cel-duration
   #:cel-x
   #:cel-y
   #:cel-opacity
   #:cel-z-index
   #:cel-type
   #:cel-user-data
   #:cel-width
   #:cel-height
   #:cel-data
   #:cel-frame-position
   #:cel-id-bitmask
   #:cel-x-flip-bitmask
   #:cel-y-flip-bitmask
   #:cel-diagonal-flip-bitmask
   ;; layer
   #:layer
   #:layer-visiblep
   #:layer-backgroundp
   #:layer-continuousp
   #:layer-referencep
   #:layer-type
   #:layer-child-level
   #:layer-blend-mode
   #:layer-opacity
   #:layer-tileset-index
   #:layer-user-data
   #:layer-cels
   ;; sprite
   #:sprite
   #:sprite-width
   #:sprite-height
   #:sprite-color-depth
   #:sprite-color-profile
   #:sprite-has-opacity-p
   #:sprite-transparent-index
   #:sprite-colors
   #:sprite-palette
   #:sprite-pixel-ratio
   #:sprite-user-data
   #:sprite-frames
   #:sprite-layers
   #:sprite-tags
   ;; functions
   #:load-sprite))
