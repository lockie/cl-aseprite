(in-package #:cl-aseprite)


(defstruct tag
  "Representation of tag.

* `FIRST-FRAME`: index of first tagged frame
* `LAST-FRAME`:  index of last tagged frame
* `DIRECTION`:   one of `:FORWARD`, `:REVERSE`, `:PING-PONG` or
`:PING-PONG-REVERSE`
* `REPEAT`:      repeat N times. 0 means infinite, 1 once etc.
* `USER-DATA`:   user data string"
  (first-frame 0   :type (unsigned-byte 16))
  (last-frame  0   :type (unsigned-byte 16))
  (direction   nil :type keyword)
  (repeat      0   :type (unsigned-byte 16))
  (user-data   ""  :type simple-string))

(defstruct cel
  "Representation of cel.

* `DURATION`:              duration in milliseconds
* `X`:                     x position of cel data
* `Y`:                     y position of cel data
* `OPACITY`:               opacity level
* `Z-INDEX`:               z-index
* `TYPE`:                  one of `:IMAGE`, `:LINKED` or `:TILEMAP`
* `USER-DATA`:             user data string
* `WIDTH`:                 width in pixels (for `:IMAGE` type) or in tiles
(for `:TILEMAP` type)
* `HEIGHT`:                height in pixels (for `:IMAGE` type) or in tiles
(for `:TILEMAP` type)
* `DATA`:                  raw data, only for `:IMAGE` and `:TILEMAP` types
* `FRAME-POSITION`:        frame position to link with (only for `:LINKED` type)
* `ID-BITMASK`:            bitmask for tile ID (only for `:TILEMAP` type)
* `X-FLIP-BITMASK`:        bitmask for X flip (only for `:TILEMAP` type)
* `Y-FLIP-BITMASK`:        bitmask for Y flip (only for `:TILEMAP` type)
* `DIAGONAL-FLIP-BITMASK`: bitmask for diagonal flip (only for `:TILEMAP` type)"
  (duration              0   :type (unsigned-byte 16))
  (x                     0   :type (signed-byte 16))
  (y                     0   :type (signed-byte 16))
  (opacity               0   :type (unsigned-byte 8))
  (z-index               0   :type (signed-byte 16))
  (type                  nil :type keyword)
  (user-data             ""  :type simple-string)
  (width                 0   :type (unsigned-byte 16))
  (height                0   :type (unsigned-byte 16))
  (data                  nil :type (or (simple-array (unsigned-byte 8)) null))
  (frame-position        0   :type (unsigned-byte 16))
  (id-bitmask            0   :type (unsigned-byte 32))
  (x-flip-bitmask        0   :type (unsigned-byte 32))
  (y-flip-bitmask        0   :type (unsigned-byte 32))
  (diagonal-flip-bitmask 0   :type (unsigned-byte 32)))

(defstruct layer
  "Representation of layer.

* `VISIBLEP`:      whether layer is visible
* `BACKGROUNDP`:   whether layer is background layer
* `CONTINUOUSP`:   whether layer is continuous (prefers linked cels)
* `REFERENCEP`:    whether layer is reference layer
* `TYPE`:          one of `:IMAGE`, `:GROUP` or `:TILEMAP`
* `CHILD-LEVEL`:   relationship with next layer in layers list, see
[here
](https://github.com/aseprite/aseprite/blob/main/docs/ase-file-specs.md#note1)
* `BLEND-MODE`:    keyword indicating [blend mode
](https://aseprite.org/api/blendmode)
* `OPACITY`:       layer opacity. only valid if `SPRITE-HAS-OPACITY-P` is `T`
* `TILESET-INDEX`: only for `:TILEMAP` type
* `USER-DATA`:     user data string
* `CELS`:          array of `CEL`s, indices correspond to frame numbers"
  (visiblep      nil :type boolean)
  (backgroundp   nil :type boolean)
  (continuousp   nil :type boolean)
  (referencep    nil :type boolean)
  (type          nil :type keyword)
  (child-level   0   :type (unsigned-byte 16))
  (blend-mode    nil :type keyword)
  (opacity       0   :type (unsigned-byte 8))
  (tileset-index 0   :type (or (unsigned-byte 32) null))
  (user-data     ""  :type simple-string)
  (cels          #() :type (simple-array (or cel null))))

(defstruct sprite
  "Representation of sprite.

* `WIDTH`:             width in pixels
* `HEIGHT`:            height in pixels
* `COLOR-DEPTH`:       color depth in bits per pixel, could be 32 (ABGR),
16 (grayscale) or 8 (indexed)
* `COLOR-PROFILE`:     `:SRGB` or `NIL` (corresponding to \"None\")
* `HAS-OPACITY-P`:     whether there is opaque color
* `TRANSPARENT-INDEX`: palette entry index for transparent color (only for 
indexed sprites)
* `COLORS`:            number of colors
* `PALETTE`:           palette as an array of packed ABGR values
* `PIXEL-RATIO`:       pixel width/pixel height
* `USER-DATA`:         user data string
* `FRAMES`:            number of frames in each layer
* `LAYERS`:            plist of sprite `LAYER`s in reversed order
* `TAGS`:              plist of frame `TAG`s"
  (width             0   :type (unsigned-byte 16))
  (height            0   :type (unsigned-byte 16))
  (color-depth       0   :type (unsigned-byte 16))
  (color-profile     nil :type (or keyword null))
  (has-opacity-p     nil :type boolean)
  (transparent-index 0   :type (unsigned-byte 8))
  (colors            0   :type (unsigned-byte 16))
  (palette           nil :type (or (simple-array (unsigned-byte 32)) null))
  (pixel-ratio       1   :type rational)
  (user-data         ""  :type simple-string)
  (frames            0   :type (unsigned-byte 16))
  (layers            nil :type property-list)
  (tags              nil :type property-list))

(declaim
 (ftype (function ((unsigned-byte 8)
                   (unsigned-byte 8)
                   (unsigned-byte 8)
                   &optional (unsigned-byte 8))
                  (unsigned-byte 32))
        pack-color)
 (inline pack-color))
(defun pack-color (r g b &optional (a 255))
  (logior
   (ash a 24)
   (ash b 16)
   (ash g 8)
   (ash r 0)))

(defun load-old-palette (chunk)
  (loop
    :with size := (loop :for packet :across
                           (core::old-palette-chunk-data-packets chunk)
                        :for size := (core::old-palette-packet-size packet)
                        :summing (if (zerop size) 256 size) :of-type fixnum)
    :with palette := (make-array size :element-type '(unsigned-byte 32))
    :for packet :across (core::old-palette-chunk-data-packets chunk)
    :for skip :of-type fixnum := (core::old-palette-packet-skip packet)
    :do (loop :for color :across (core::old-palette-packet-colors packet)
              :for i :of-type fixnum :from 0
              :do (setf (aref palette (+ i skip))
                        (pack-color (core::old-palette-color-r color)
                                    (core::old-palette-color-g color)
                                    (core::old-palette-color-b color))))
    :finally (return palette)))

(defun load-layer (chunk)
  (values
   (make-layer
    :visiblep      (plusp (core::layer-chunk-data-visible chunk))
    :backgroundp   (plusp (core::layer-chunk-data-background chunk))
    :continuousp   (plusp (core::layer-chunk-data-continuous chunk))
    :referencep    (plusp (core::layer-chunk-data-reference chunk))
    :type          (core::layer-chunk-data-type chunk)
    :child-level   (core::layer-chunk-data-child-level chunk)
    :blend-mode    (core::layer-chunk-data-blend-mode chunk)
    :opacity       (core::layer-chunk-data-opacity chunk)
    :tileset-index (core::layer-chunk-data-tileset-index chunk))
   (core::layer-chunk-data-name chunk)))

(declaim (ftype (function (core::cel-chunk-data)
                          (values cel (unsigned-byte 16)))
                load-cel))
(defun load-cel (chunk)
  (let* ((data (core::cel-chunk-data-data chunk))
         (cel (make-cel
               :x       (core::cel-chunk-data-x-position chunk)
               :y       (core::cel-chunk-data-y-position chunk)
               :type    :unknown
               :opacity (core::cel-chunk-data-opacity chunk)
               :z-index (core::cel-chunk-data-z-index chunk))))
    (case (core::cel-chunk-data-type chunk)
      (:raw
       (setf (cel-type   cel) :image
             (cel-width  cel) (core::raw-cel-data-width  data)
             (cel-height cel) (core::raw-cel-data-height data)
             (cel-data   cel) (core::raw-cel-data-data   data)))
      (:linked
       (setf (cel-type           cel) :linked
             (cel-frame-position cel) (core::linked-cel-data-position data)))
      (:compressed-image
       (setf (cel-type   cel) :image
             (cel-width  cel) (core::compressed-image-cel-data-width  data)
             (cel-height cel) (core::compressed-image-cel-data-height data)
             (cel-data   cel) (core::compressed-image-cel-data-data   data)))
      (:compressed-tilemap
       (setf (cel-type cel)
             :tilemap
             (cel-width cel)
             (core::compressed-tilemap-cel-data-width data)
             (cel-height cel)
             (core::compressed-tilemap-cel-data-height data)
             (cel-id-bitmask cel)
             (core::compressed-tilemap-cel-data-id-bitmask data)
             (cel-x-flip-bitmask cel)
             (core::compressed-tilemap-cel-data-x-flip-bitmask data)
             (cel-y-flip-bitmask cel)
             (core::compressed-tilemap-cel-data-y-flip-bitmask data)
             (cel-diagonal-flip-bitmask cel)
             (core::compressed-tilemap-cel-data-diagonal-flip-bitmask data))))
    (values cel (core::cel-chunk-data-layer-index chunk))))

(defun load-color-profile (chunk)
  (when (eq (core::color-profile-chunk-data-type chunk) :srgb)
    :srgb))

(defun load-tags (chunk)
  (loop :for tag         :across (core::tags-chunk-data-tags chunk)
        :for first-frame := (core::tag-first-frame tag)
        :for last-frame  := (core::tag-last-frame tag)
        :for direction   := (core::tag-direction tag)
        :for repeat      := (core::tag-repeat tag)
        :for name        := (core::tag-name tag)
        :nconcing        (list
                          (format-symbol :keyword "~:@(~A~)" name)
                          (make-tag :first-frame first-frame
                                    :last-frame  last-frame
                                    :direction   direction
                                    :repeat      repeat))))

(declaim (ftype (function (core::palette-chunk-data
                           (or null (simple-array (unsigned-byte 32))))
                          (simple-array (unsigned-byte 32)))
                load-palette))
(defun load-palette (chunk palette)
  (loop
    :with size := (core::palette-chunk-data-size chunk)
    :with palette := (or palette
                         (make-array size :element-type '(unsigned-byte 32)))
    :with first := (core::palette-chunk-data-first-index chunk)
    :for entry :across (core::palette-chunk-data-entries chunk)
    :for i :of-type fixnum :from 0
    :do (setf (aref palette (+ i first))
              (pack-color (core::palette-entry-r entry)
                          (core::palette-entry-g entry)
                          (core::palette-entry-b entry)
                          (core::palette-entry-a entry)))
    :finally (return palette)))

(defun load-userdata (chunk)
  (if-let ((text-data (core::user-data-chunk-data-text chunk)))
    (core::text-user-data-text text-data)
    ""))

(defun load-sprite (filespec)
  "Loads Aseprite file from given `FILESPEC`, either path or binary stream.

See `SPRITE`."
  (let+ (((&values stream close-stream-p)
          (etypecase filespec
            ((or string pathname)
             (values (open filespec :element-type '(unsigned-byte 8)) t))
            (stream
             (values filespec nil))))
         (file (read-binary 'core::file stream)))
    (when close-stream-p
      (close stream))
    (loop
      :with palette          := nil
      :with color-profile    := nil
      :with last-object      := nil
      :with sprite-user-data := ""
      :with nframes          := (core::file-frames-size file)
      :with layers           := nil
      :with tags             :of-type list := nil
      :for frame       :across (core::file-frames file)
      :for frame-index :of-type fixnum :from 0
      :for duration    := (core::frame-duration frame)
      :do (loop
            :for chunk :of-type core::chunk :across
               (core::frame-chunks frame)
            :for chunk-data := (core::chunk-data chunk)
            :do (case (core::chunk-type chunk)
                  (:old-palette
                   (unless palette
                     (setf palette (load-old-palette chunk-data)))
                   (setf last-object nil))
                  (:layer
                   (let+ (((&values layer name) (load-layer chunk-data))
                          (key (format-symbol :keyword "~:@(~A~)" name)))
                     (setf (layer-cels layer)
                           (make-array nframes :element-type    '(or cel null)
                                               :initial-element nil)
                           layers
                           (nconc layers (list key layer))
                           last-object
                           layer)))
                  (:cel
                   (let+ (((&values cel layer-index) (load-cel chunk-data))
                          (layer (nth (1+ (* layer-index 2)) layers)))
                     (setf (cel-duration cel)                    duration
                           (aref (layer-cels layer) frame-index) cel
                           last-object                           cel)))
                  (:color-profile
                   (setf color-profile (load-color-profile chunk-data)
                         last-object   nil))
                  (:tags
                   (setf tags        (load-tags chunk-data)
                         last-object tags))
                  (:palette
                   (setf palette     (load-palette chunk-data palette)
                         last-object nil))
                  (:user-data
                   (let ((userdata (load-userdata chunk-data)))
                     (typecase last-object
                       (null  (setf sprite-user-data userdata))
                       (layer (setf (layer-user-data last-object) userdata))
                       (cel   (setf (cel-user-data last-object) userdata))
                       (list  (setf (tag-user-data (second last-object))
                                    userdata
                                    last-object (cddr last-object))))))))
      :finally (return
                 (make-sprite
                  :width             (core::file-width file)
                  :height            (core::file-height file)
                  :color-depth       (core::file-color-depth file)
                  :color-profile     color-profile
                  :has-opacity-p     (plusp (logand (core::file-flags file) 1))
                  :transparent-index (core::file-transparent-index file)
                  :colors            (core::file-colors file)
                  :palette           palette
                  :pixel-ratio       (let ((pw (core::file-pixel-width file))
                                           (ph (core::file-pixel-height file)))
                                       (if (or (zerop pw) (zerop ph))
                                           1
                                           (/ pw ph)))
                  :user-data         sprite-user-data
                  :frames            (core::file-frames-size file)
                  :layers            layers
                  :tags              tags)))))
