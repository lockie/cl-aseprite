(defsystem "cl-aseprite"
  :version "0.1.2"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "GPLv3"
  :homepage "https://gitlab.com/lockie/cl-aseprite"
  :depends-on (#:lisp-binary #:chipz #:trivial-types #:alexandria #:let-plus)
  :serial t
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "core")
                 (:file "interface"))))
  :description "Aseprite file format parser"
  :in-order-to ((test-op (test-op "cl-aseprite/tests"))))

(defsystem "cl-aseprite/tests"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "MIT"
  :depends-on (#:cl-aseprite #:parachute)
  :serial t
  :components ((:module "tests"
                :components
                ((:file "package")
                 (:file "main")
                 (:file "tests"))))
  :description "Test system for cl-aseprite"
  :perform (test-op (op c) (uiop:symbol-call :cl-aseprite/tests '#:run)))
